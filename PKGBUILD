# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=boost
pkgver=1.79.0
pkgrel=1
pkgdesc="Free peer-reviewed portable C++ source libraries development headers"
arch=('x86_64')
url="https://www.boost.org/"
license=('custom')
depends=('bzip2' 'zlib' 'zstd' 'which' 'icu')
makedepends=('python')
source=(https://boostorg.jfrog.io/artifactory/main/release/${pkgver}/source/${pkgname}_${pkgver//./_}.tar.bz2)
sha256sums=(475d589d51a7f8b3ba2ba4eda022b170e562ca3b760ee922c146b6c65856ef39)

build() {
    cd ${pkgname}_${pkgver//./_}

    ./bootstrap.sh             \
        --prefix=/usr          \
        --libdir=/usr/lib64    \
        --with-python=python3  \
        --with-icu             \
        --with-toolset=gcc

    ./b2 stage -j$(nproc)                    \
        threading=multi                      \
        link=shared                          \
        runtime-link=shared                  \
        toolset=gcc                          \
        python=3.10                          \
        cflags="${CPPFLAGS} ${CFLAGS} -O3"   \
        linkflags="${LDFLAGS}"
}

package() {
    cd ${pkgname}_${pkgver//./_}

    ./b2 install -j$(nproc)                  \
        threading=multi                      \
        link=shared                          \
        runtime-link=shared                  \
        toolset=gcc                          \
        python=3.10                          \
        cflags="${CPPFLAGS} ${CFLAGS} -O3"   \
        linkflags="${LDFLAGS}"               \
        --prefix=${pkgdir}/usr               \
        --libdir=${pkgdir}/usr/lib64
}
